import {Component, OnInit} from '@angular/core';
import {HttpService} from 'src/app/services/http.service';

import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss']
})
export class MetricsComponent implements OnInit {

  select = false;
  dashboardname;
  metrics = [];
  tabname1;
  tabname2;
  getsearch;
  searchResult = [];

  constructor(private http: HttpService, private _nav: Router,private toastr: ToastrService) {
  }

  ngOnInit() {
    this.get_metrics_dasboard();

    // $( '#datepicker' ).datepicker();
  }


  get_metrics_dasboard() {

    this.http.get_metrics().subscribe(
      res => {

        this.metrics = res;
        console.log(res.title);
        this.dashboardname = res.title;
        this.tabname1 = res.viewOptions[0].name;
        this.tabname2 = res.viewOptions[1].name;
      });
  }

  setFilter(item) {
    this.getsearch = '';
    this.searchResult = [];
    const data = {
      value: item.id
    };

    this.http.setFilter(data).subscribe(res => {
      console.log(res);
    });
  }

  selectPerios(period) {

    this.select = false;
    this.http.setPeriod(period.id).subscribe(res => {
      console.log(res);
    });
  }

  selectViews(viewId) {

    this.select = false;
    this.http.setView(viewId).subscribe(res => {
      console.log(res);
    });
  }

  search(query) {
    let auth = {
      value: query

    };
    this.http.search(auth).subscribe(
      res => {
        console.log(res);

        this.searchResult = res;

      });
  }

  logout() {

    this.http.logout().subscribe(
      data => {
        console.log(data);
        
        this.toastr.success(data['response']);
          this._nav.navigate(['/login']);
        
        localStorage.removeItem('role')
        localStorage.removeItem('clienttag');

      });
  }
}
